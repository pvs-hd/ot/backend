# This is just an example to get you started. You may wish to put all of your
# tests into a single file, or separate them into multiple `test1`, `test2`
# etc. files (better names are recommended, just make sure the name starts with
# the letter 't').
#
# To run these tests, simply execute `nimble test`.

import unittest, os, db_sqlite

import core/[database]

let databaseFile = "tests/database/testdb.sqlite"

suite "database setup tests":
  createDir("tests/database")

  setup:
    let db = open(databaseFile,"","","")

  teardown:
    db.close()

  test "can setup database":
    db.setupDatabase()
    assert fileExists(databaseFile)
    assert db.getAllRows(sql"""select name from sqlite_master where type = 'table'""") ==
      @[@["role"], @["user"], @["session"], @["project"], @["master"], @["file"]]

  removeDir("tests/database")

suite "database init test":

  createDir("tests/database")

  test "can init database":
    initDatabase(databaseFile)
    assert fileExists(databaseFile)

  removeDir("tests/database")