import unittest, os, db_sqlite

import core, core/private/auth

let
  databaseFile = "tests/database/testdb.sqlite"
  csvFile = "tests/testdata/task2_data.csv"
var
  t1: Table
  session: string

suite "tb1 test":
  createDir("tests/database")
  initDatabase(databaseFile)

  setup:
    let db = open(databaseFile,"","","")

  teardown:
    db.close()

  test "create user with user object":
    assert fileExists(databaseFile)
    let user = newUser("Max Mustermann", "maxmus", "mm@email.de", "swordfish", 2)
    db.createUser(user)
    assert db.tryExec(sql"SELECT * FROM user")
    assert db.userExists("maxmus")
    assert not db.userExists("peter")
    assert db.getRow(sql"SELECT * FROM user") ==
      Row(@["1", "Max Mustermann", "maxmus", "mm@email.de", encrypt("swordfish"), "2"])

  test "checkUserLogin":
    assert db.isCorrectPassword("maxmus", "swordfish")
    assert not db.isCorrectPassword("maxmus", "test")

  test "login user":
    session = db.login("maxmus", "swordfish", "000.000.000.000")
    assert db.sessionStatus(session, "000.000.000.000") == "active"

  test "logout user":
    db.logout(session, "000.000.000.000")
    assert db.sessionStatus(session, "000.000.000.000") == "logged out"

  test "create project":
    db.createProject("test2", "tests/database/test2.sqlite")
    assert db.projectExists("test2")

  test "import file to filestructure":
    db.writeFileToDatabase(csvFile, "maxmus")
    assert db.fileExists(csvFile)

  test "create table from file":
    t1 = createTableFromCsv(csvFile)
    assert(t1.name == "task2_data")
    assert(t1.columnnames == @["first_name", "last_name", "email", "credit_card_no", "expires_at"])
    assert(t1.content[0] == @["Toyah", "Booker", "toyah.booker@nsw.gov.au", "2222405343248877", "01/28"])
    assert(t1.content.len == 21)

  test "import table to database":
    db.writeNewTableToDatabase(t1)
    assert(db.tableExists("task2_data"))
    assert(not db.tableExists("table_not_exist"))

  test "get sample table data - TBI":
    discard

  test "do transform on table - TBI":
    discard

  test "show result of transform - TBI":
    discard

  test "get full table data":
    assert(db.getTable("task2_data").content.len == 21)

  test "get part of table data":
    assert(db.head("task2_data", 6).content.len == 6)
    assert(db.getSlice("task2_data", 2..6).content.len == 5)

  test "save script - TBI":
    discard
    ##let script = Script(name: "test_name", steps: @[
    ##  Mapping(tablename: "tableX", mapText: "Script of the first mapping"),
    ##  Mapping(tablename: "tableY", mapText: "Script of the second mapping"),
    ##  Mapping(tablename: "tableZ", mapText: "Script of the third mapping")])
    ##for mapping in script.steps:
    ##  db.saveMapping(mapping)
    ##db.saveScript(script)
    ##let loadedScript = db.getScript("test_name")
    ##assert(loadedScript.name == "test_name")
    ##assert(loadedScript.steps[0].tablename == "tableX")
    ##assert(loadedScript.steps[0].mapText == "Script of the first mapping")
    ##assert(loadedScript.steps[0].tablename == "tableY")
    ##assert(loadedScript.steps[0].mapText == "Script of the second mapping")
    ##assert(loadedScript.steps[0].tablename == "tableZ")
    ##assert(loadedScript.steps[0].mapText == "Script of the third mapping")

  test "undo - TBI":
    discard

  test "export data to file - TBI":
    discard

  removeDir("tests/database")