type
  User* = ref object
    name*, username*, email*, password, passhash*: string
    role*: int