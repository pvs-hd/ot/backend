import unittest, db_sqlite, os

import core

let databaseFile = "tests/database/testdb.sqlite"

suite "create Tables test":
  createDir("tests/database")
  initDatabase(databaseFile)

  setup:
    let db = open(databaseFile,"","","")

  teardown:
    db.close()

  test "t1":
    let t1 = Table(name: "test", columnnames: @["a","b"], columntypes: @["text","text"], content: @[@["11","12"],@["21","22"]])
    db.writeNewTableToDatabase(t1)
    assert tableExists(db, t1.name)
    assert db.getRow(sql"SELECT name FROM master")[0] == "test"
    db.deleteTable(t1.name)
    assert not tableExists(db, t1.name)
    assert db.getRow(sql"SELECT name FROM master")[0] == ""

  removeDir("tests/database")
