import unittest, os, db_sqlite, json, strutils

import core

let databaseFile = "tests/database/testdb.sqlite"

suite "tb1 test":
  createDir("tests/database")
  initDatabase(databaseFile)

  setup:
    let db = open(databaseFile,"","","")

  teardown:
    db.close()

  test "get part of table data":
    let t1: Table = createTableFromCsv("tests/testdata/task2_data.csv")
    db.writeNewTableToDatabase(t1)
    assert pretty(% db.head("task2_data", 2)) ==
      dedent """
      {
        "name": "task2_data",
        "columnNames": [
          "first_name",
          "last_name",
          "email",
          "credit_card_no",
          "expires_at"
        ],
        "columnTypes": [
          "text",
          "text",
          "text",
          "text",
          "text"
        ],
        "content": [
          [
            "Toyah",
            "Booker",
            "toyah.booker@nsw.gov.au",
            "2222405343248877",
            "01/28"
          ],
          [
            "Allegra",
            "Owens",
            "allegra.owens@cargocollective.com",
            "2222990905257051",
            "06/31"
          ]
        ],
        "creator": ""
      }"""

  removeDir("tests/database")