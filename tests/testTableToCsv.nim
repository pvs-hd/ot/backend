import unittest

import core/table

test "create csv string from table object":
  let t1 = Table(name: "t1", columnnames: @["1","2,"], columntypes: @["1","2"], content: @[@["11","12"],
                   @["2\"1","2\n2"]])
  assert t1.toCsvString() == "1,\"2,\"\n11,12\n\"2\"\"1\",\"2\n2\""