import nimcrypto/scrypt, strutils, os, random
from math import pow

const n = int(pow(2'f32,15))

proc makeSalt(): string =
  ## creates a random salt string
  randomize()
  for i in 1..32:
      result.add(char(rand(int('a') .. int('z'))))

proc makeSaltCompileTime(): string =
  ## creates a random salt string at compile time.
  var rng = initRand(0x13DE1E1DB34F)
  for i in 1..32:
      result.add(char(rng.rand(int('z')) + int('a')))

# const salt = makeSaltCompileTime()

func scrypt[T, M](password: openArray[T], salt: openArray[M],
                   N, r, p, keyLen: static[int]): array[keyLen, byte] =
  let (xyvLen, bLen) = scryptCalc(N, r, p)
  var xyv = newSeq[uint32](xyvLen)
  var b = newSeq[byte](bLen)
  discard scrypt(password, salt, N, r, p, xyv, b, result)

proc `$`(bytes: array[0..31, byte]): string=
  for byte in bytes:
    result &= toHex[byte](byte)

proc encrypt*(password: string): string =
  ## makes a passhash from the password and salt with scrypt
  var salt = ""
  if fileExists("salt"):
    salt = readFile("salt")
  else:
    salt = makeSalt()
    writeFile("salt", salt)
  result = $scrypt(password=password, salt=salt, N=n, r=8, p=1, keyLen=32)