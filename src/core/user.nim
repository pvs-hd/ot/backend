import private/auth

include model/user

proc newUser*(name, username, email, password: string, role:int =0): User =
  ## hashes the password with the salt and returns a new User with the hash
  let passhash = encrypt(password)
  result = User(name: name, username: username, email: email, passhash: passhash, role: role)

proc `$`* (user: User): string =
  result = "Name: '" & user.name & "' Username: '" & user.username & "' E-Mail: '" & user.email &
   "' Hash: '" & user.passhash & "' Role: '" & $user.role & "'"

if isMainModule:
  assert $newUser("test", "test1", "test2", "test3", 2) ==
    "Name: 'test' Username: 'test1' E-Mail: 'test2'"&
    " Hash: 'F744A98E33494F51A2BB015B35DB150BD5B0DF67B9BF8B2B42EE235368165E85' Role: '2'"