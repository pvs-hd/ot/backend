import user

type
  File* = ref object
    name: string
    creator*: User
    content*: string