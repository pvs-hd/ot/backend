import db_sqlite, os

from ../core import createProject

proc setupRoleTable(db: DbConn) =
  const query = sql"""CREATE TABLE IF NOT EXISTS role(
                                  role_id INTEGER PRIMARY KEY,
                                  rolename text,
                                  rights text)"""
  db.exec(query)

proc setupUserTable(db: DbConn) =
  const query = sql"""CREATE TABLE IF NOT EXISTS user(
                        user_id INTEGER PRIMARY KEY,
                        name text,
                        username text,
                        email text,
                        passhash text,
                        role integer,
                        FOREIGN KEY (role) REFERENCES role(role_id) ON UPDATE CASCADE ON DELETE SET NULL)"""
  db.exec(query)

proc setupSessionTable(db: DbConn) =
  const query = sql"""CREATE TABLE IF NOT EXISTS session(
                        session_id INTEGER PRIMARY KEY,
                        username text,
                        cookie text,
                        ip_address text,
                        status text DEFAULT 'active',
                        timestamp datetime DEFAULT CURRENT_TIMESTAMP,
                        FOREIGN KEY (username) REFERENCES user(username) ON UPDATE CASCADE ON DELETE SET NULL)"""
  db.exec(query)

proc setupProjectTable(db: DbConn) =
  const query = sql"""CREATE TABLE IF NOT EXISTS project(
                        project_id INTEGER PRIMARY KEY,
                        name text,
                        path text)"""
  db.exec(query)

proc setupMasterTable(db: DbConn) =
  const query = sql"""CREATE TABLE IF NOT EXISTS master(
                        table_id INTEGER PRIMARY KEY,
                        name text,
                        date_created date,
                        date_modified date,
                        creator integer,
                        project integer,
                        table_type text,
                        FOREIGN KEY (creator) REFERENCES user(user_id) ON UPDATE CASCADE ON DELETE SET NULL,
                        FOREIGN KEY (project) REFERENCES project(project_id) ON UPDATE CASCADE ON DELETE CASCADE)"""
  db.exec(query)

proc setupFileTable(db: DbConn) =
  const query = sql"""CREATE TABLE IF NOT EXISTS file(
                        file_id INTEGER PRIMARY KEY,
                        name text,
                        creator integer,
                        content blob,
                        FOREIGN KEY (creator) REFERENCES user(user_id) ON UPDATE CASCADE ON DELETE SET NULL)"""
  db.exec(query)

proc setupDatabase*(db: DbConn) =
  setupRoleTable(db)
  setupUserTable(db)
  setupSessionTable(db)
  setupProjectTable(db)
  setupMasterTable(db)
  setupFileTable(db)

proc initDatabase*(path: string)=
  ## initializes the master database at the path
  let db = open(path, "", "", "")
  setupDatabase(db)
  let pathSplit = splitPath(path)
  createProject(db, "default", pathSplit.head / "default.sqlite")
  db.close()
