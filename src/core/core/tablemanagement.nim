## This module provides the functionality to manage tables
##
## :Copyright: 2021 OpenTable
##
import db_sqlite, times, strutils, sequtils
import ../table, ../exceptions, projectmanagement

proc columnstring(t: Table): string =
  ## Creates a string for table creation: ? ?,\n which will turn into columnname columntype,\n
  result = "? ?,\n".repeat(t.columnnames.len - 1) & "? ?"

proc questionmarkstring*(t: Table): string =
  ## Creates a string for data instertion: ?, ?, ?...
  result = "?, ".repeat(t.columnnames.len - 1) & "?"

proc tableExists*(db: DbConn, tableName: string, projectname = "default"): bool =
  ## Returns whether a table exists or not.
  const query = sql"""SELECT count(*) FROM master WHERE name=? AND project=?"""
  let projectId = db.getProjectInfo(projectname).id
  return db.getRow(query, tableName, projectId)[0] != "0"

proc writeTableInfoToDatabase(db: DbConn, table: Table, projectname = "default") =
  ## Insert informations about a table into the 'master table' of the database
  const query = sql"""INSERT INTO master(name, date_created, project)
                      VALUES (?, ?, ?)"""
  let projectId = db.getProjectInfo(projectname).id
  db.exec(query, table.name, $now(), projectId)

proc createTable(db: DbConn, table:Table) =
  ## Creates a Table in the database from a Table object with the types from columntpes and the columnnames
  let query = sql("""CREATE TABLE ?(""" & table.columnstring & ")")
  var args = @[table.name]
  for i in 0..<table.columntypes.len:
    args.add(table.columnnames[i])
    args.add(table.columntypes[i])
  db.exec(query, args)

proc writeTableContentToDatabase*(db: DbConn, table: Table) =
  ## Write a table object including its content to the database
  let query =  sql("INSERT INTO ?(" & questionmarkstring(table) & ") VALUES (" & questionmarkstring(table) & ")")
  for row in table.content:
    let arguments = concat(@[table.name], table.columnnames, row)
    db.exec(query, arguments)

proc writeNewTableToDatabase*(db: DbConn, table: Table, projectname = "default") 
  {.raises: [tableExistError, projectExistsError, ValueError, DbError]} =
  ## Creates a new entry in the master table, creates a new table in the database and
  ## populates it with all data from the table object. As default it will safe the table to projecct default
  if tableExists(db, table.name):
    raiseTableDoesExistError(table.name)
  db.writeTableInfoToDatabase(table, projectname)
  let projectdb = openProject(db, projectname)
  projectdb.createTable(table)
  projectdb.writeTableContentToDatabase(table)
  projectdb.close()

proc deleteTable*(db: DbConn, tablename: string, projectname = "default") = 
  ## Delete a table from the database.
  ##
  ## **CAUTION** This action is reversible
  if not tableExists(db, tablename):
    raiseTableDoesNotExistError(tablename)
  const queryForMasterTable = sql"DELETE FROM master WHERE name = ?"
  const queryDropTable = sql"DROP TABLE ?"
  db.exec(queryForMasterTable, tablename)
  let projectdb = openProject(db, projectname)
  projectdb.exec(queryDropTable, tablename)
  projectdb.close()

proc exportTableToFile*(filename: string, t: Table){.deprecated: "use table toCsvString instead"}=
  echo "Deprecated Function exportTableToFile, use table toCsvString instead"