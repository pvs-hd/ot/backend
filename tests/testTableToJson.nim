import unittest, json

import core/table

test "create json from table object with only columnnames and name":
  let t1 = Table(name: "t1", columnnames: @["1","2"])
  let jobj = %t1
  assert($jobj == """{"name":"t1","columnNames":["1","2"],"columnTypes":[],"content":[],"creator":""}""")

test "create json from table object":
  let t1 = Table(name: "t1", columnnames: @["1","2"], columntypes: @["1","2"], content: @[@["11","12"],
                   @["21","22"]])
  let jobj = %t1
  assert($jobj ==
    """{"name":"t1","columnNames":["1","2"],"columnTypes":["1","2"],"content":[["11","12"],["21","22"]],"creator":""}""")

# test "create json from table object with dates":
#   let t1 = Table(name: "t1", columnnames: @["1","2"], columntypes: @["1","2"],
#                  content: @[@["11","12"], @["21","22"]],
#                  dateCreated: parse("2021-01-01 +0", "yyyy-MM-dd z").utc, datemodified: parse("2021-01-02 +0", "yyyy-MM-dd z").utc)
#   let jobj = %t1
#   assert($jobj ==
#     """{"Name":"t1","Columnnames":["1","2"],"Columntypes":["1","2"],"Content":[["11","12"],["21","22"]],"Date Created":"2021-01-01T00:00:00Z","Date Modified":"2021-01-02T00:00:00Z"}""")