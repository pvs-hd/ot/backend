import user

type
  Role* = ref object
    name*: User
    rights*: seq[tuple[name: string, setting: bool]]