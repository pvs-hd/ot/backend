type
  backendError = object of CatchableError

  userError = object of backendError
  userExistError* = object of userError
  userRightsError* = object of userError
  credentialsError* = object of userError
  encryptionError* = object of credentialsError
  sessionError* = object of userError
  cookieError* = object of sessionError
  ipAddressError* = object of sessionError

  databaseError = object of backendError
  tableExistError* = object of databaseError
  columnExistError* = object of databaseError
  projectExistsError* = object of databaseError
  fileExistsError* = object of backendError

proc raiseTableDoesNotExistError*(tablename: string) =
  raise newException(tableExistError, "Table " & tablename & " does not exist")

proc raiseTableDoesExistError*(tablename: string) =
  raise newException(tableExistError, "Table " & tablename & " does already exist")

proc raiseColumnDoesNotExistError*(columnname: string) =
  raise newException(columnExistError, "Column " & columnname & " does not exist")

proc raiseFileDoesNotExistError*(filename: string) =
  raise newException(fileExistsError, "File " & filename & " does not exist")