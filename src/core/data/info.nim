## This module provides the functionality to return data and information 
## about the structure of a table.
##
## :Author: Frederick Vandermoeten, David Jäck, Christopher Höllriegl
## :Copyright: 2021 OpenTable
##
import json, db_sqlite, strutils, strformat
import ../table, ../core, ../exceptions

proc columnExists*(db: DbConn, tablename: string, columnname: string, projectname = "default"): bool =
  ## Returns whether a column of a given table exists or not
  let projectdb = openProject(db, projectname)
  const query = sql"""SELECT count(*) FROM pragma_table_info(?) WHERE name=?"""
  result = projectdb.getRow(query, tablename, columnname)[0] != "0"
  projectdb.close()

proc getTableInfo*(db: DbConn, tablename: string, projectname = "default") : tuple[names, types: seq[string]] =
  ## Returns the strucure of a table as a tuple
  ## A table with the following structure  
  ## 
  ## > id (INTEGER)  
  ## > name (TEXT)  
  ## > car (BLOB)  
  ##   
  ## would return following tuple:
  ##
  ## (['id', 'name', 'car'], ['integer', 'text', 'blob'])
  if not tableExists(db, tablename, projectname):
    raiseTableDoesNotExistError(tablename)

  let projectdb = openProject(db, projectname)

  const query = sql"""PRAGMA table_info(?)"""
  var info : seq[seq[string]]
  var names : seq[string]
  var types : seq[string]

  for x in projectdb.fastRows(query, tablename):
      result.names.add(x[1])
      result.types.add(x[2])
  projectdb.close()

proc getColumnNames*(db: DbConn, tablename: string, projectname = "default") : seq[string] =
  ## Returns a string sequence of column names for the given `table`
  return db.getTableInfo(tablename, projectname)[0]

proc getColumnTypes*(db: DbConn, tablename: string, projectname = "default") : seq[string] =
  ## Returns a string sequence of column types for the given `table`
  return db.getTableInfo(tablename, projectname)[1]

proc getTable*(db: DbConn, tablename: string, projectname = "default", orderby="") : Table=
  ## Returns a table object of the table `tablename`  
  ## **Caution:** This object includes **ALL** the data in the table
  if not tableExists(db, tablename, projectname):
    raiseTableDoesNotExistError(tablename)

  let projectdb = openProject(db, projectname)

  let query = sql("SELECT * FROM ?" & (if orderby=="": "" else: " ORDER BY ?"))
  let info = db.getTableInfo(tablename, projectname)

  result = Table(name: tablename, columnnames: info[0], columntypes: info[1],
                 content: projectdb.getAllRows(query, tablename, orderby))
  projectdb.close()

proc getTableWithRowId*(db: DbConn, tablename: string, projectname = "default", orderby="") : Table=
  ## Returns a table object of the table `tablename`
  ## **Caution:** This object includes **ALL** the data in the table including row ids
  if not tableExists(db, tablename, projectname):
    raiseTableDoesNotExistError(tablename)

  let projectdb = openProject(db, projectname)

  let query = sql("SELECT *, rowid FROM ?" & (if orderby=="": "" else: " ORDER BY ?"))
  let info = db.getTableInfo(tablename, projectname)

  result = Table(name: tablename, columnnames: info[0], columntypes: info[1],
                 content: projectdb.getAllRows(query, tablename, orderby))
  projectdb.close()

proc getSlice*(db: DbConn, tablename: string, slize: HSlice, projectname = "default", orderby="") : Table=
  ## Returns a slice of data from the table `tablename` including the table structure as a Table object
  ## `slice` can be any value range as HSlice (ex: `1..7`)
  if not tableExists(db, tablename, projectname):
    raiseTableDoesNotExistError(tablename)

  let projectdb = openProject(db, projectname)

  let query = sql("SELECT * FROM ? " & (if orderby=="": "" else: "ORDER BY ? ") & "LIMIT ? OFFSET ?")
  let startrow = slize.a - 1
  let endrow = slize.b
  let info = db.getTableInfo(tablename, projectname)

  result = Table(name: tablename, columnnames: info[0], columntypes: info[1],
                 content: projectdb.getAllRows(query, tablename, endrow-startrow, startrow, orderby))
  projectdb.close()

proc head*(db: DbConn, tablename: string, endrow = 5, projectname = "default", orderby="") : Table=
  ## Returns a slice of data from the table `tablename` including the table structure as a Table object  
  ##    
  ## `head()` always begins with the first row.  
  ## If no `endrow` parameter is given it gets assigned `5` by default. 
  return getSlice(db, tablename, 1..endrow, projectname, orderby)

proc getTableWithoutContent*(db: DbConn, tablename: string, projectname = "default") : Table=
  ## Returns a table object of the table `tablename`
  ## The table object doesn't include any data, only the table structure
  let info = db.getTableInfo(tablename, projectname)

  result = Table(name: tablename, columnnames: info[0], columntypes: info[1])

proc getRowCount*(db: DbConn, tablename: string, projectname = "default") : int=
  ## Returns the number of rows the table `tablename` contains
  if not tableExists(db, tablename, projectname):
    raiseTableDoesNotExistError(tablename)

  let projectdb = openProject(db, projectname)

  const query = sql"""SELECT count(*) FROM ? """

  result = parseInt(projectdb.getRow(query, tablename)[0])
  projectdb.close()

proc getTableColumn*(db:DbConn, tablename, columnname: string, projectname = "default"): seq[Row]=
  ## Returns the data of a colum as a sequence of Rows, inlcuding row id 
  ## and the data.
  ##   
  ## **Caution:** This object includes **ALL** the data of the column
  if columnExists(db, tablename, columnname):
    raiseColumnDoesNotExistError(columnname)

  let projectdb = openProject(db, projectname)

  const query = sql("SELECT rowid, ? FROM ?")
  result = projectdb.getAllRows(query, columnname, tablename)
  projectdb.close()