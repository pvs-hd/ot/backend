import db_sqlite

import model/project

from core/projectmanagement import getProjectInfo

proc getProject(db: DbConn, projectname: string= "default"): Project=
  let projtuple = getProjectInfo(db, projectname)
  result = new(Project)
  result.name = projtuple.name
  result.path = projtuple.path
  result.id = projtuple.id

proc `$`* (p: Project): string =
  result = "name: '" & p.name & "', path: " & $p.path & ", id: " & $p.id

if isMainModule:
  let db = open("database/master.sqlite","","","")
  echo (getProject(db))
