import macros, action

macro addPlugin(plugin: static[string]): untyped=
  let module = ident(plugin)
  result = quote do:
    import `module`

macro handleMessage(plugin: static[string], message: string): untyped=
  let module = ident(plugin)
  result = quote do:
    `module`.handleRequest(message)

addPlugin("examplePlugin")
if isMainModule:
  let message = "Content-Length: 68\c\n\c\n{\n  \"jsonrpc\": \"2.0\",\n  \"id\": 2,\n  \"method\": \"examplePlugin/stuff\"\n}"
  handleMessage("examplePlugin", message)