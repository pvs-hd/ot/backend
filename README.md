# OpenTable Core
OpenTable Core is the core api of the [OpenTable](https://gitlab.com/pvs-hd/ot) project.
It uses the [Nim programming language](https://nim-lang.org) and [SQLite](https://www.sqlite.org)

## Contents
- [Installation](#installation)
- [Usage](#usage)
- [Documentation](#documentation)
- [About](#about)

## Installation
Install module with:
>nimble install https://gitlab.com/pvs-hd/ot/core.git

Install in-dev features:
>nimble install https://gitlab.com/pvs-hd/ot/core.git@#head
## Usage
Commands added by nimble:<br/>
| | |
|--|--|
|nimble test            | Runs all tests in the [tests](/tests) directory starting with the letter t|
|nimble build/install   | Builds/installs the package                                               |
|nimble %task%          | Runs the corresponding task from the [core.nimble](core.nimble) file|
## Documentation
DocGen html [link](https://pvs-hd.gitlab.io/ot/core/index.html)

Weekly progress updates are provided inside the directory [presentations](/presentations)

Follow up documentation and accounting are in [documentation](/documentation)

Backlog in [Trello](https://trello.com/invite/beopentable/e6bb0daf2c03a70b9404db1da4307850)
## About
OpenTable Backend has been written by [Frederick Vandermoeten](https://gitlab.com/add-IV),
[Christopher Höllriegl](https://gitlab.com/buddiman) and [David Jäck](https://gitlab.com/david.jaeck.15.06)
