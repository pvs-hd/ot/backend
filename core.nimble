# Package

version       = "0.3.2"
author        = "Frederick Vandermoeten, Christopher Höllriegl, David Jäck"
description   = "Core for the OpenTable project"
license       = "MIT"
srcDir        = "src"


# Dependencies

requires "nim >= 1.4.6", "nimcrypto", "jsonschema", "https://gitlab.com/add-IV/exampleplugin"

task hello, "testing tasks":
  echo("hi")

task document, "generates documentation":
  exec "nim doc --project --index:on --outdir:public src/core.nim"
  exec "nim buildIndex -o:public/index.html public"
