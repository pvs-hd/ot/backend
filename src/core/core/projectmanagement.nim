## This module provides the functionality to manage projects
##
## :Copyright: 2021 OpenTable
##
import db_sqlite, strutils, os
import ../exceptions

proc projectExists*(db: DbConn, projectname: string): bool =
  ## Returns whether a project with a given name exists
  const query = sql"""SELECT count(*) FROM project WHERE name=?"""
  return db.getRow(query, projectname)[0] != "0"

proc createProject*(db: DbConn, projectname, path: string) =
  ## Create a new project with a specified project name and database path
  if projectExists(db, projectname):
    raise newException(projectExistsError, "Project does already exist")
  const query = sql"""INSERT INTO project(name, path) VALUES (?, ?)"""
  db.exec(query, projectname, path)
  open(path, "", "", "").close()

proc getProjectInfo*(db: DbConn, projectname: string): tuple[id: int, name, path: string] =
  ## Get informations about a given project 
  ## 
  ## This function returns the following informations:
  ## 
  ## > id   (int)     (identification number of the project)
  ## > name (string)  (name of the project)
  ## > path (string)  (path of the database file of the project)
  if not projectExists(db, projectname):
    raise newException(projectExistsError, "Project does not exist")
  const query = sql"""SELECT project_id, name, path FROM project WHERE name = ?"""
  let response = db.getRow(query, projectname)
  result.id = parseInt response[0]
  result.name = response[1]
  result.path = response[2]

proc deleteProject*(db: DbConn, projectname: string) =
  ## Delete a given project if it exists
  ## 
  ## **Caution:** Possible data loss, the corresponding database file gets deleted
  if not projectExists(db, projectname):
    raise newException(projectExistsError, "Project does not exist")
  removeFile(getProjectInfo(db, projectname).path)
  const query = sql"""DELETE FROM project WHERE name = ?"""
  db.exec(query, projectname)

proc openProject*(db: DbConn, projectname: string): DbConn=
  let projectPath = db.getProjectInfo(projectname).path
  result = open(projectPath, "", "", "")