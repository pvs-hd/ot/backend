## This module provides the functionality to manage files
##
## :Copyright: 2021 OpenTable
##
import streams, db_sqlite
import usermanagement, ../exceptions

proc writeFileToDatabase*(db: DbConn, filename, creator: string)=
  ## Stores a file and its creator in the database
  const query = sql"""INSERT INTO file(name, creator, content) VALUES (?, ?, ?)"""
  let stream = newFileStream(filename, mode = fmRead)
  defer: stream.close()
  let content = stream.readAll()
  let user_id = $getUserId(db, creator)
  db.exec(query, filename, user_id, content)

proc fileExists*(db: DbConn, filename: string): bool=
  ## Returns whether a file with a given name exists
  const query = sql"""SELECT count(*) FROM file WHERE name=?"""
  return db.getRow(query, filename)[0] != "0"

proc getFileFromDatabase*(db: DbConn, filename: string): string=
  ## Returns a file with the given name from the database
  if db.fileExists(filename) == false:
    raiseFileDoesNotExistError(filename)
  const query = sql"""SELECT content FROM file WHERE name = ?"""
  result = db.getRow(query, filename)[0]