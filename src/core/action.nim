import json

include model/action

proc createMessage*(action: RequestMessage | ResponseMessage | NotificationMessage): string =
  let content = pretty action.JsonNode
  let length = content.len
  result = "Content-Length: " & $length & "\r\n\r\n" & content

if isMainModule:
  let data = parseJson("""{"key": 3.14}""")
  echo createMessage create(ResponseMessage, "2.0", 2, some(data), none(ResponseError))