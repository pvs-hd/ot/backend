# Follow Up Documentation

## Contents
- [Repo Structure](#repo-structure)
    - [Main Directories](#main-directories)
    - [Src Folder](#src-folder)
- [Data Structures](#data-structures)
- [Feature Extension](#feature-extension)

## Repo Structure

### Main Directories
The main directories of the repository are:

- database:   
contains an example of a masterDB and a projectDB as well as some code to generate them
for use with e.g. IntelliJ's database functionality
- documentation
- presentations 
- src
- tests

### Src Folder
Inside the src folder there is the following structure:
- core.nim: exports all  needed modules
- core/data.nim, core/database.nim, core/core.nim: exports the modules of their respective folder
- core/model folder: contains model of all data structure
- core/action.nim, core/project.nim, core/table.nim, core/user.nim: implementation of data structures
- core/core/ folder: functionality for file, user, project and table-management respectively
- core/database folder: code for setup of database
- data/info.nim: code for getting data from the database
- core/private/auth.nim: code for passhash generation

## Data Structures

The data structures correspond to the database objects:

<details><summary>Click to show database</summary>
![database](/presentations/diagrams/database.png)
</details>

- action: a jrpc action (currently not used)
- file: a file with a name and some content
- project: a project corresponding to a database file
- role: a role for user rights management
- table: main data structure:
    - name: table name
    - creator: table creator
    - columnNames: names of columns
    - columnTypes: types of columns
    - content: seq of table rows
- user: a user with a hashed password

## Feature Extension

Possible feature extensions are:

- jrpc: change the project from a library to an executable.
  This is already explored on some feature branches as well as in action.nim.
- plugins: add plugin support for easier code additions.
- user rights system: check user rights before executing a request, part of the executable update.
- different sql dialects: add a dialect field to project data structure, change some functions in info.nim and tablemanagement.nim
