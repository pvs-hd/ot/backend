# this file is used to create an emtpy database file inside this directory, to be used with intellij's database tab

import os, db_sqlite

import ../src/backend, ../src/backend/core, ../src/backend/table, ../src/backend/model/project

let databaseFile = "database/master.sqlite"
let csv = "testdata/task2_data.csv"

if fileExists(databaseFile):
  removeFile(databaseFile)

initDatabase(databaseFile)
let db = open(databaseFile, "", "", "")
let t1 = createTableFromCsv(csv)
db.writeNewTableToDatabase(t1)
db.close()
