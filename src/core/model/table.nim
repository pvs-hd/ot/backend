type
  Table* = ref object
    name*: string
    creator*: string
    columnNames*: seq[string]
    columnTypes*: seq[string]
    content*: seq[seq[string]]