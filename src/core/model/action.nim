# implements lsp 3.16
# https://microsoft.github.io/language-server-protocol/specifications/specification-current/#contentPart
import jsonschema, json, options

jsonSchema:
  Message:
    jsonrpc: string

  RequestMessage extends Message:
    id: int or string
    "method": string
    params?: any[] or any

  ResponseMessage extends Message:
    id: int or string or nil
    "result"?: any
    error?: ResponseError

  ResponseError:
    code: int
    message: string
    data: any

  NotificationMessage extends Message:
    "method": string
    params?: any[] or any
