import json, strutils, parsecsv, os, streams

include model/table

proc `%`* (t: Table): JsonNode =
  ## creates a JSON object with all nonempty Fields of the Table
  result = newJObject()
  result.add("name", %t.name)
  result.add("columnNames", %t.columnnames)
  result.add("columnTypes", %t.columntypes)
  result.add("content", %t.content)
  result.add("creator", %t.creator)

proc `$`* (t: Table): string =
  result = "name: '" & t.name & "', columnNames: " & $t.columnnames & ", columnTypes: " & $t.columntypes

proc indexOf*(t: Table, columnname: string): int {.inline.} =
  ## given a table returns the index of a column
  return t.columnNames.find(columnname)

proc containsSpecialCharacter(str: string, delimiter=','): bool =
  ## checks if string contains one of the special characters of the csv specification
  let specialChars = {'"', '\n', '\r', delimiter}
  if str.contains(specialChars):
    return true
  return false

func encodeQuotes(str: string): string =
  result = str.replace("\"", "\"\"")

proc createCsvLine(row: seq[string], delimiter=','): string =
  ## turns one row into an csv line
  result = ""
  for element in row:
    if containsSpecialCharacter(element, delimiter):
      result &= '"' & encodeQuotes(element) & '"' & delimiter
    else:
      result &= element & delimiter
  result.delete(result.len-1,result.len)

proc toCsvString*(t:Table): string =
  ## turns an table to a csv string
  result = createCsvLine(t.columnNames)
  for row in t.content:
    result &= '\n' & createCsvLine(row)

proc createTableFromCsv*(csvfile: string, seperator = ',', hasHeaders= true): Table =
  ## creates table object based on a csv file, sets type as text for all columns
  var p: CsvParser
  let tableName= splitFile(csvfile).name
  result = Table(name: tableName)
  p.open(csvfile, seperator)
  if hasHeaders:
    p.readHeaderRow()
  while p.readRow():
    result.content.add(p.row)
  if hasHeaders:
    result.columnnames = p.headers
    for i in 0..<p.headers.len:
      result.columntypes.add("text")

proc createTableFromCsvString*(csvstring:string, tablename="", seperator = ',', hasHeaders= true): Table =
  ## creates table object based on a csv string, sets type as text for all columns
  var p: CsvParser
  result = Table(name: tablename)
  let strm = newStringStream(csvstring)
  p.open(strm, tablename, seperator)
  if hasHeaders:
    p.readHeaderRow()
  while p.readRow():
    result.content.add(p.row)
  if hasHeaders:
    result.columnnames = p.headers
    for i in 0..<p.headers.len:
      result.columntypes.add("text")