## This module provides the functionality to manage users
##
## :Copyright: 2021 OpenTable
##
import db_sqlite, md5, strutils, times
import ../user, ../private/auth, ../exceptions

proc userExists*(db: DbConn, username: string): bool =
  ## Returns whether a given user exists
  const query = sql"""SELECT count(*) FROM user WHERE username = ?"""
  return db.getRow(query, username)[0] != "0"

proc getUserId*(db: DbConn, username: string): int =
  ## Returns the id of a given user
  if not userExists(db, username):
    raise newException(userExistError, "User does not Exist")
  const query = sql"""SELECT user_id FROM user WHERE username = ?"""
  return parseInt db.getRow(query, username)[0]

proc createUser*(db: DbConn, user: User)
  {.raises: [userExistError, DbError]} =
  ## Creates a new user in the database
  if userExists(db, user.username):
    raise newException(userExistError, "User " & user.username & " does already exist")
  const query = sql"""INSERT INTO user(name, username, email, passhash, role) VAlUES(?,?,?,?,?)"""
  db.exec(query, user.name, user.username, user.email, user.passhash, user.role)

proc isCorrectPassword*(db: DbConn, username: string, password: string): bool
  {.raises: [DbError, userExistError, encryptionError]}=
  ## Checks user login by comparing it to the hash in database, doesn't raise an error if user does not exists
  const query =  sql"""SELECT passhash FROM user WHERE username = ?"""
  let hash = db.getRow(query, username)
  var expected: string
  if hash.len == 0:
    raise newException(userExistError, "User doesn't exist")
  try:
    expected = encrypt(password)
  except IOError:
    raise newException(encryptionError, "Could not encrypt given password")
  if expected == hash[0]:
    return true
  return false

proc sessionStatus*(db: DbConn, cookie, ip_address: string): string
  {.raises: [sessionError, DbError]}=
  ## Gets the session status for a cookie and ip_address
  const query = sql"SELECT ip_address, status FROM session WHERE cookie = ?"
  let session = db.getRow(query, cookie)
  if session.len == 0:
    raise newException(cookieError, "Session Cookie does not exist")
  if session[0] != ip_address:
    raise newException(ipAddressError, "Ip Address doesn't match Session")
  return session[1]

proc login*(db: DbConn, username, password, ip_address: string): string
  {.raises: [userExistError, credentialsError, DbError]}=
  ## Creates a session if a user exists, returns the new cookie
  if not db.userExists(username):
    raise newException(userExistError, "Incorrect username or password")
  if not db.isCorrectPassword(username, password):
    raise newException(credentialsError, "Incorrect username or password")
  const query = sql"INSERT INTO session(username, cookie, ip_address) VALUES (?, ?, ?)"
  let cookie = getMD5(username&ip_address & $toUnix(getTime()))
  db.exec(query, username, cookie, ip_address)
  result = cookie

proc logout*(db: DbConn, cookie, ip_address: string)=
  ## Changes the session status to logged out for the session
  let status = db.sessionStatus(cookie, ip_address)
  if status != "active":
    raise newException(sessionError, "Session is not active")
  const query = sql"UPDATE session SET status = 'logged out' WHERE cookie = ?"
  db.exec(query, cookie)
